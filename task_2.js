const user = {
    name: "Your Name",
    address: {
        personal: {
            line1: "101",
            line2: "street Line",
            city: "NY",
            state: "WX",
            coordinates: {
                x: 35.12,
                y: -21.49,
            },
        },
        office: {
            city: "City",
            state: "WX",
            area: {
                landmark: "landmark",
            },
            coordinates: {
                x: 35.12,
                y: -21.49,
            },
        },
    },
    contact: {
        phone: {
            home: "xxx",
            office: "yyy",
        },
        other: {
            fax: '234'
        },
        email: {
            home: "xxx",
            office: "yyy"
        },
    },
};

let oFlatten = {}

getFlattenObject = (user, name) => {
    for (const key in user) {
        if (typeof user[key] === 'object') {
            // const element = user[key];
            // console.log(element)
            getFlattenObject(user[key], name + "_" + key);
        } else {
            // console.log(key + ": " + user[key]);
            oFlatten[name + "_" + key] = user[key]
        }
    }
};


getFlattenObject(user, 'user');
console.log(oFlatten)