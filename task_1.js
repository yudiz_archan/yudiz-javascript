
var nAccountBalance = 1000;
var nSpendingLimit = 800;
const nGSTtax = 0.10;
const nMobilePrice = 100;
const nAccessoriesPrice = 50;

var nAccessoriesCount = 0;
var nPhonesCount = 0;
var nTotalAmount = 0;

function addGST(total) {
    return total + (total * nGSTtax);
}

if (nSpendingLimit > nAccountBalance) {
    console.log("Spending limit is gretaer than Account Balance")
} else {
    while (((nTotalAmount + nMobilePrice + addGST(nMobilePrice)) < nSpendingLimit)) {
        nTotalAmount = nMobilePrice + addGST(nMobilePrice);
        nPhonesCount = nPhonesCount + 1;

        if ((nTotalAmount + nAccessoriesPrice + addGST(nAccessoriesPrice)) < nSpendingLimit) {
            nTotalAmount = nAccessoriesPrice + addGST(nAccessoriesPrice);
            nAccessoriesCount = nAccessoriesCount + 1;
            console.log(nPhonesCount)
        }
    }
    console.log('Starting Bank Balance: ' + nAccountBalance);
    nAccountBalance -= nTotalAmount;
    console.log('Phones Purchased: ' + nPhonesCount);
    console.log('Accessories Purchased: ' + nAccessoriesCount);
    console.log('Total Purchase Amount: ' + nTotalAmount);
    console.log('Ending Bank Balance: ' + nAccountBalance);
}



